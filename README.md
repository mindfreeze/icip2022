# ICIP 2022 Supplymentary Material

## Frame-type sensitive RDO Control for Content-Adaptive Encoding

Author: Vibhoothi, Anil Kokaram, François Pitié 

Contact:  vibhoothi@tcd.ie, anil.kokaram@tcd.ie, pitief@tcd.ie

### Table of Contents 
[[_TOC_]]
### Abstract 
Video transcoding is an increasingly important application in the streaming
media industry. It has become important to investigate the optimisation of
transcoder parameters for a single clip simply because of the immense number of
playbacks for popular clips. In this paper we explore the use of a canned
optimiser to estimate the optimal RD tradeoff achievable for a particular clip.
We show that by adjusting the Lagrange multiplier in RD optimisation on
keyframes alone we can achieve more than 10x the previous BD-Rate gains
possible without affecting quality for any operating point in the Random-Access Preset.

### Dataset

| Clip Name                                      	| Set         	| Resolution 	| Framerate 	| Bitdepth 	| Color Format 	|
|------------------------------------------------	|-------------	|------------	|-----------	|----------	|--------------	|
| CoverSong_1080P-1b0c                           	| YouTube-UGC 	| 1920x1080  	| 29.724    	| 10       	| 4:2:0        	|
| DinnerSceneCropped_1920x1080_2997fps_10bit_420 	| av2-a2-2k   	| 1920x1080  	| 29.97     	| 10       	| 4:2:0        	|
| FoodMarket_1920x1080_5994_10bit_420            	| av2-a2-2k   	| 1920x1080  	| 59.94     	| 8        	| 4:2:0        	|
| Lecture_1080P-3ce0                             	| YouTube-UGC 	| 1920x1080  	| 50        	| 8        	| 4:2:0        	|
| MusicVideo_1080P-04b6                          	| YouTube-UGC 	| 1920x1080  	| 23.976    	| 10       	| 4:2:0        	|
| OldTownCross_1920x1080p50                      	| av2-a2-2k   	| 1920x1080  	| 50        	| 8        	| 4:2:0        	|
| Riverbed_1920x1080p25                          	| av2-a2-2k   	| 1920x1080  	| 25        	| 10       	| 4:2:0        	|
| Skater227_1920x1080_30fps                      	| av2-a2-2k   	| 1440x1080  	| 30        	| 8        	| 4:2:0        	|
| TelevisionClip_1080P-39e3                      	| YouTube-UGC 	| 1920x1080  	| 25        	| 8        	| 4:2:0        	|
| Vlog_1080P-58ad                                	| YouTube-UGC 	| 1920x1080  	| 29.97     	| 8        	| 4:2:0        	|

The Non-Pristine clips were selected manually by subjective-evaluation and
content-type,

Samples of each clip is below, and they can be also found inside
[data/gifs](data/gifs).

| ![](data/gifs/CoverSong_1080P-1b0c.y4m.gif)                           | ![](data/gifs/Lecture_1080P-3ce0.y4m.gif)                  |
|-----------------------------------------------------------------------|------------------------------------------------------------|
| ![](data/gifs/DinnerSceneCropped_1920x1080_2997fps_10bit_420.y4m.gif) | ![](data/gifs/FoodMarket_1920x1080_5994_10bit_420.y4m.gif) |
| ![](data/gifs/MusicVideo_1080P-04b6.y4m.gif)                          | ![](data/gifs/OldTownCross_1920x1080p50.y4m.gif)           |
| ![](data/gifs/Riverbed_1920x1080p25.y4m.gif)                          | ![](data/gifs/Skater227_1920x1080_30fps.y4m.gif)           |
| ![](data/gifs/TelevisionClip_1080P-39e3.y4m.gif)                      | ![](data/gifs/Vlog_1080P-58ad.y4m.gif)                     |

The y4m files for these clips can be found from, i) [AOM-CTC
sets](https://media.xiph.org/video/aomctc/test_set/a2_2k/) ii) [YouTube-UGC](https://media.withyoutube.com/).

### Encoder Configuration

libaom-av1:
``` sh
$AOMEMC --cpu-used=0 --passes=1 --lag-in-frames=19 --auto-alt-ref=1 --min-gf-interval=16 --max-gf-interval=16
--gf-min-pyr-height=4 --gf-max-pyr-height=4 --limit=130 --kf-min-dist=65 --kf-max-dist=65 --use-fixed-qp-offsets=1
--deltaq-mode=0 --enable-tpl-model=0 --end-usage=q --cq-level=$Q --enable-keyframe-filtering=0 --threads=1
--test-decode=fatal -o output.ivf $OPTIONS $INPUT.Y4M
```
> GOP-Size is fixed at 65, Sub-GOP is 16, Temporal-layers is 4, Closed-GOP
GOP-Size is set in AV1 with help of `kf-min-dist` and `kf-max-dist`. Sub-GOP
size is set in AV1 using `min-gf-interval` and `max-gf-interval`. The temporal
layers is defined using `gf-min-pyr-height` and `gf-max-pyr-height`. To have a
hierarchical reference structure, the number of temporal layers should be set to
$log2(SubGopsize)$. Open-GOP and Closed GOP is controlled with help of `
--enable-fwd-kf` inside AV1.


HEVC-HM:
```sh
TAppEncoderStatic -i $INPUT.Y4M -c encoder_randomaccess_main.cfg --FramesToBeEncoded=130 --SourceWidth=1920
--SourceHeight=1080 --FrameRate=$FPS --InputBitDepth=8 --OutputBitDepth=8 --QP=$Q -b $OUTPUT.bin $OPTIONS
```
> GOP-Size is fixed at 16, Rate-control turned off,B slice = 15, SliceMode
> disabled.

### Quantization Parameters(QP)

|SL-No| libaom-av1 | hevc-hm |
|-----|------------|---------|
| 1   |27          | 22      |
| 2   |39          | 27      |
| 3   |49          | 32      |
| 4   |59          | 37      |
| 5   |63          | 42      |

The QP points were chosen based on the AOM-Common Testing Configuration(CTC.), 
and JVET-CTC. For libaom-av1, they were closely tried to match with hevc-hm.

### Experiment Setup

#### *Tuning Options*
As mentioned in the paper, we have been doing this experiment in two-levels,
1. At the Top-level where the $\lambda$ is adjusted for all the Encoding decisions.
2. At the Partition-level, where the $\lambda$ is adjusted only during the
Partiton-search.

We chose (2.) based on the Encoding impact/use of the $\lambda$ inside the Encoder
for AV1.

There were 4 different modes for each of the levels being carried out in AV1 and
3 in HEVC-HM.

SL No | AV1 | HEVC-HM
-- | -- | --
1 | All-Frames | All
2 | KF Only | I-Frames Only
3 | GF/ARF Only | B-Frames Only
4 | KF & GF/ARF Only |  


#### *Encoder Modifications*
In, AV1 and HEVC, the lambda/$\lambda$ adjustments are not exposed by default. We have
modifed both Encoders to support feeding the $\lambda$ values externally, and
inside the encoder, we adjust the $\lambda$ values based on the given encoding
conditions and frame-types. To control tuning only at Partiton level we have
added additional flag to control that.


#### *Optimiser Setup*
For this experiment, we are relying on Brent's optimiser which is implemented
inside Scipy's Optmise
module's([scipy.optimize.fminbound](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.fminbound.html)),
we are using bounded verison of that, where we provided upper and lower borund
of the possible lambda adjustments which was noticed from previous
years experiments. So it was choosen to be between 0 and 4.
The default maximum iteration is set to be at 30. Most of the times, we require
only to do 6-10 unique iterations/k-values.


#### Data Collection

The per clip data-point for each QP is collected with the help of
[libvmaf](https://github.com/Netflix/vmaf/tree/master/libvmaf) module of
[VMAF](https://github.com/Netflix/vmaf/),
Table below shows the metrics being collected for each runs, 

| SL No | Metrics Name | SL No | Metrics Name  |
|-------|--------------|-------|---------------|
| 1     | PSNR Y       | 9     | PSNR-HVS Cb   |
| 2     | PSNR Cb      | 10    | PSNR-HVS Cr   |
| 3     | PSNR Cr      | 11    | Encoding Time |
| 4     | PSNR-HVS     | 12    | Decoding Time |
| 5     | CIEDE2000    | 13    | VMAF          |
| 6     | SSIM         | 14    | VMAF-NEG      |
| 7     | MS-SSIM      | 15    | APSNR Y       |
| 8     | PSNR-HVS Y   | 16    | APSNR Cb      |
|       |              | 17    | APSNR Cr      |

### Experimental Results

Inside [data](data/), you can find per run type mode results, the files are
inside [csv](data/csv) folder.
The genral format of each file is `ugc-av2-2k-$LEVEL-s0-t$MODE-CODEC.csv`, where 

`$LEVEL` indicates where the $\lambda$ is modified(top or part), 

`$MODE` indicates the tuning mode deployed inside the codec to get the results,

`$CODEC` indicates the codec(av1 or hevc).

The results of the Optimiser is found inside the [json](data/json) folder.  The
general schema of the JSON data is shown below:
<details>

  ```json
  {
    "run_info": {
        "set"  :  "$RUN_NAME",
        "run_id"  :  "$RUN_ID",
        "extra_options"  :  "",
        "qualities"  :  [5],
        "cost_method"  :  "$METHOD",
		"optimiser_mode"  :  "$OPT_MODE",
		"lambda_opt"  :  true,
		"codec"  :  "av1",
		"metrics"  :  "$METRICS"
        },
	"clip_name": {
	    "brent_data"  : [4],
        "total_encodes"  :	$number,
	    "bdrate_data"  :  {
                            "$k_value" : { "$bdrate_values_metrics" }
                          },
	    "cost_pair"  :  {
                           "$k_value" : "$bdrate"
                        }
  }
  ```

</details>


### RD-Curves

The RD-Curves for each clip is available inside the [plots](data/plots) folder,
where there is two folders one is [pchip](data/plots/pchip) version of the graph, where the
datapoints are interpolated with help of Piecewise Cubic Hermite Interpolating 
Polynomial (PCHIP) using
[Matlab](https://uk.mathworks.com/help/matlab/ref/pchip.html).
This same method is also used for computation of the BD-Rate for the optmiser.
Second one is normal RD-Curve in Linear Scale insdie [rd](data/plots/rd) folder.

Both of the Folder is having, pdf files with Video Name as title, and each File
have the RD-Curve for 12 Different metirc as defined in the Table above. Both of
the folder also ahve a folder called `full_dump` which has per-clip per-metric
graphs in `eps` format for further analysis.


### Summary


Run-Level | Frame-Type | Avg. K Value | BD-Rate(%) Average (MS-SSIM) | BD-Rate(%) Maximum (MS-SSIM) | BD-Rate(%) Minimum (MS-SSIM) | Avg. Unique Iterations | Mean Bitrate Savings(%) | Q39 Bitrate Savings(%) | Q39 Bitrate(kb/s) | Avg. MS-SSIM Change(dB) | Avg. VMAF Change
-- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | --
Top-level | All Frames | 1.247 | -0.539 | -1.546 | 0.261 | 8.700 | -1.635 | -5.243 | 4411.054 | 0.327 | 1.342
Top-level | KF only | 2.878 | -3.832 | -24.757 | -0.392 | 9.100 | -3.458 | -8.575 | 4587.438 | 0.026 | 0.075
Top-level | GF, ARF only | 2.226 | -1.841 | -4.027 | 0.152 | 8.700 | -4.603 | -5.173 | 4606.677 | 0.097 | 0.170
Top-level | KF, GF, ARF only | 2.494 | -4.924 | -29.159 | -0.202 | 10.000 | -5.817 | -10.503 | 4494.956 | 0.151 | 0.832
Partition-level | All Frames | 1.203 | -0.492 | -1.637 | 0.190 | 9.600 | -1.569 | -4.337 | 4469.403 | 0.269 | 1.145
Partition-level | KF only | 2.748 | -3.692 | -23.763 | -0.381 | 9.000 | -3.318 | -8.394 | 4565.141 | 0.023 | 0.082
Partition-level | GF, ARF only | 2.149 | -1.754 | -3.897 | 0.149 | 8.900 | -4.306 | -4.761 | 4594.510 | 0.085 | 0.137
Partition-level | KF, GF, ARF only | 2.467 | -4.772 | -29.049 | 0.000 | 9.500 | -4.618 | -8.761 | 4520.424 | 0.127 | 0.708

<div align="center">
Table 1: Average of 10 clips at each levels for different frame types in <i>AV1</i>
</div>


Top-level | I & B Frames | 0.964 | -0.097 | -0.323 | 0.077 | 9.700 | 2.232 | 1.520 | 3922.118 | -0.045 | -0.210
-- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | --
Top-level | I-frames only | 2.051 | -1.458 | -5.552 | 0.038 | 9.300 | -3.260 | -4.671 | 3745.858 | 0.070 | 0.431
Top-level | B-frames only | 0.867 | -0.682 | -4.271 | 0.022 | 7.500 | 14.671 | 4.228 | 4023.768 | -0.124 | -0.704
Partition-level | I & B Frames | 0.942 | -0.143 | -0.625 | 0.078 | 7.900 | -0.726 | 0.933 | 3909.959 | -0.031 | -0.104
Partition-level | I-frame only | 1.467 | -0.495 | -2.513 | 0.058 | 9.500 | 0.627 | -1.201 | 3841.400 | 0.009 | 0.063
Partition-level | B-frames only | 0.954 | -0.339 | -1.806 | 0.042 | 9.500 | -2.803 | 1.440 | 3947.856 | -0.059 | -0.314

<div align="center">
Table 2: Average of 10 clips at each levels for different frame types in <i>HEVC</i>
</div>

### DinnerSceneCropped Clip Analysis

This specific clip in our experiments were giving the best gains, we have seen a
massive drop in the bitrates for this specific clip ranging upto 67%. To
investigate further, we have imported the clip to the bitstream analyzer
[AOMAnaylzer](https://github.com/xiph/aomanalyzer)
[[Link](https://beta.arewecompressedyet.com/analyzer/?maxFrames=5&decoder=https://media.xiph.org/analyzer/inspect.js&decoderName=dinner_baseline_39&file=https://mindfreeze.tk/~mindfreeze/DinnerSceneCropped_1920x1080_2997fps_10bit_420.y4m-39.ivf&decoderName=dinner_whole_t5&file=https://mindfreeze.tk/~mindfreeze/DinnerSceneCropped_1920x1080_2997fps_10bit_420.y4m-39-3.79.ivf)].

Below Images show the Heat-map distrubution Frame-Relative(First
Frame/Key-Frame) for this clip,

Source             |  Best Result
:-------------------------:|:-------------------------:
![](data/img/plane_frame1.png)  |  ![](data/img/plane_frame2.png)
Transform and Split | Transform and Split 
![](data/img/transform_frame1.png)  |  ![](data/img/transform_frame2.png)
Heat Map | Heat Map 
![](data/img/frame1.png)  |  ![](data/img/frame2.png)
Single-Block| Single-Block
![](data/img/patch_frame1.png)  |  ![](data/img/patch_frame2.png)


## Acknowledgements 

This Work is part of Video Intelligence Search Platform(VISP) Project, which is
funded by Enterprise Ireland under Disruptive Technology Innovation Fund
(DTIF.), Grant No, DT-2019-0068.

We would also like to thank Sigmedia.tv members, and people from
Open-source community and AOMedia members in Industry for giving feedbacks on
the implementation overtime.