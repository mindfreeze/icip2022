%close all;
%full_table = readtable("ugc-av2-2k-dinner-rd.csv");
full_table = readtable("ugc-av2-2k-rd-summary.csv");

codecs = ['av1','hevc'];
run_types = {'whole','part'};
markers = ['o' '+' '*' '.' 'x' '_' '|' 's' 'd' '^' 'v' '>' '<' 'p' 'h'];
line_styles = {'-' '--' ':' '-.'};
style_index = 1;
marker_index = 1;
legend_list = string(10);
%video_set = unique(full_table.Video);
video_set = {'DinnerSceneCropped_1920x1080_2997fps_10bit_420.y4m'};
%video_set = video_set(1:3);
figure_handler = 2;
for codec = {'av1', 'hevc'}
for k = 1:length(video_set)

f = figure(figure_handler);
%f.Visible = 'off';
set(f, 'PaperPositionMode', 'auto');
video = video_set{k};
base_table = full_table((strcmp(full_table.run_mode,'base') & ...
                strcmp(full_table.codec, codec) & ...
                strcmp(full_table.Video,video)),:);
legend_list(1) = 'Base Run';
spline_base = min(base_table.Bitrate_kbps_(2:5)):5:max(base_table.Bitrate_kbps_(2:5));
real_spline = pchip(base_table.Bitrate_kbps_(2:5), base_table.PSNRY(2:5), spline_base);
%spline_base =  min(base_table.Bitrate_kbps_(:)):100:max(base_table.Bitrate_kbps_(:));
%real_spline = pchip(base_table.Bitrate_kbps_(:), base_table.MS_SSIM(:), spline_base);
p = plot(spline_base,real_spline);%, ...
    %'LineStyle',line_styles(style_index));
%p = plot((base_table.Bitrate_kbps_(:)),base_table.PSNRY(:), ...
%    'LineStyle',line_styles(style_index));
array_index = 2;
style_index = 2;
hold on;
    if strcmp(codec,'av1')
        modes = [1,2,3,5];
        %modes = [1,2,5];
    else
        modes = [1,2,4];
    end
for run_type = {'whole'}%, 'part'}
for i = modes
    run_marker = append(run_type, '-t',string(i));
    if strcmp(run_type, 'whole')
        run_mode = 'Whole';
    else 
        run_mode = 'Part';
    end
    %run_mode = append(run_type, '-t',string(i));
    this_table = full_table((strcmp(full_table.run_mode, run_marker) & ...
        strcmp(full_table.codec, codec) & ...
        strcmp(full_table.Video,video)), :);
    %marker_indices = this_table.PSNRY;
    if strcmp(codec, 'av1')
        if i == 1
            legend_name = 'All Frames';
        elseif i == 2
            legend_name = 'KF Only';
        elseif i == 3
            legend_name = 'GF, ARF Only';
        elseif i == 5
            legend_name = 'KF, GF, ARF Only';
        end
    else
        if i == 1
            legend_name = 'I & B Frames';
        elseif i == 2
            legend_name = 'I Frames Only';
        elseif i == 4
            legend_name = 'B Frames Only';
        end
    end
    legend_list(array_index) = legend_name; %append(run_mode,' at ',legend_name);% ...
                    %string(this_table.K_Value(1)));
    this_spline =  min(this_table.Bitrate_kbps_(2:5)):5:max(this_table.Bitrate_kbps_(2:5));
    this_real_spline = pchip(this_table.Bitrate_kbps_(2:5), this_table.PSNRY(2:5), this_spline);
    %this_spline =  min(this_table.Bitrate_kbps_(:)):100:max(this_table.Bitrate_kbps_(:));
    %this_real_spline = pchip(this_table.Bitrate_kbps_(:), this_table.MS_SSIM(:), this_spline);
    p = plot(this_spline, this_real_spline);
%    hold on;
    %p = plot(this_spline(2:uint8(length(this_spline)/5):end), this_real_spline(2:uint8(length(this_real_spline)/5):end), Marker=markers(marker_index));
    %p = plot(this_table.Bitrate_kbps_(:), this_table.PSNRY(:));
    %p.Marker = markers(style_index);
   % p.MarkerIndices = marker_indices;
    %p.Marker = markers(style_index);
    if strcmp(run_type, 'part')
    p.LineStyle = line_styles(style_index);%'--';
    end
    hold on;
    array_index = array_index +1;
    style_index = style_index +1;
    marker_index  = marker_index  +1;
    if style_index >4
        style_index = 1;
    end
end
hold off;
end
legend(legend_list, Location='southeast');
xlabel("Bitrate (kb/s)");
ylabel("PSNR-Y (dB)");
plotname = sprintf("plots/icip-%s-%s.eps",video,codec{:});
title(append(video,'-', codec),'Interpreter', 'none');
axis tight;
ax = gca;
ax.FontSize = 14;
ax.XMinorTick = 'on';
ax.YMinorTick = 'on';
hold off;
%daspect([1 0.5 2])
%exportgraphics(gcf,plotname,"Resolution",300);
figure_handler  = figure_handler  +1;
end
hold off;
end