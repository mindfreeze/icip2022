%close all;
%full_table = readtable("ugc-av2-2k-dinner-rd.csv");
full_table = readtable("ugc-av2-2k-rd-summary.csv");

codecs = ['av1','hevc'];
run_types = {'whole','part'};
markers = ['o' '+' '*' '.' 'x' '_' '|' 's' 'd' '^' 'v' '>' '<' 'p' 'h'];
line_styles = {'-' '--' ':' '-.'};
style_index = 1;

legend_list = string(10);
video_set = unique(full_table.Video);

video_set = video_set(1:3);
figure_handler = 1;
for codec = {'av1', 'hevc'}
for k = 1:length(video_set)

f = figure(figure_handler);
f.Visible = 'off';
set(f, 'PaperPositionMode', 'auto');
video = video_set{k};
base_table = full_table((strcmp(full_table.run_mode,'base') & ...
                strcmp(full_table.codec, codec) & ...
                strcmp(full_table.Video,video)),:);
legend_list(1) = 'base-run';
p = plot((base_table.Bitrate_kbps_(:)),base_table.PSNRY(:), ...
    'LineStyle',line_styles(style_index));
array_index = 2;
style_index = 2;
hold on;
    if strcmp(codec,'av1')
        %modes = [1,2,3,5];
        modes = [1,2,5];
    else
        modes = [1,2,4];
    end
for run_type = {'whole', 'part'}
for i = modes
    run_mode = append(run_type, '-t',string(i));
    this_table = full_table((strcmp(full_table.run_mode, run_mode) & ...
        strcmp(full_table.codec, codec) & ...
        strcmp(full_table.Video,video)), :);
    legend_list(array_index) = append(run_mode,'-', ...
                    string(this_table.K_Value(1)));
    p = plot((this_table.Bitrate_kbps_(:)), ...
        this_table.PSNRY(:));
    p.LineStyle = line_styles(style_index);%'--';
    hold on;
    array_index = array_index +1;
    style_index = style_index +1;
    if style_index >4
        style_index = 1;
    end
end
hold on;
end
legend(legend_list, Location='southeast');
xlabel("Bitrate(kb/s)");
ylabel("PSNR(dB)");
plotname = sprintf("plots/%s-%s.eps",video,codec{:});
title(append(video,'-', codec),'Interpreter', 'none');
axis tight;
hold off;
%daspect([1 0.5 2])
exportgraphics(gcf,plotname,"Resolution",300);
figure_handler  = figure_handler  +1;
end
end

